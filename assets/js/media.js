// Dropzone
Dropzone.autoDiscover = false;

if ($('#demo-upload').length > 0) {
    var dropzone = new Dropzone('#demo-upload', {
      url: $('#demo-upload').prop('action'),
      previewTemplate: document.querySelector('#preview-template').innerHTML,
      parallelUploads: 2,
      thumbnailHeight: 120,
      thumbnailWidth: 120,
      maxFilesize: 3,
      filesizeBase: 1000,
      thumbnail: function(file, dataUrl) {
        if (file.previewElement) {
          file.previewElement.classList.remove("dz-file-preview");
          var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
          for (var i = 0; i < images.length; i++) {
            var thumbnailElement = images[i];
            thumbnailElement.alt = file.name;
            thumbnailElement.src = dataUrl;
          }
          setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
        }
      }

    });
}

$(function(){
    $('.select-pict').click(function(){
        var fieldID = $('[name="field"]').val();
        if(fieldID != ''){
            var parentField = parent.$('#'+fieldID);
            parentField.val( $(this).data('id') );
        }else{
            //souce drom tinymce
            var activeEditor = parent.tinyMCE.activeEditor;
            activeEditor.insertContent('<img src="'+ $(this).data('url') +'" style="max-width: 100%;"/>');
        }
        parent.$.fancybox.close();
    });
});
