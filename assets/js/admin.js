$(function(){
    //TinyMCE
    //$('.tinymce').tinymce();
    tinyMCE.init({
        selector:'.tinymce',
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar: [
            "bold italic strikethrough underline bullist numlist blockquote hr alignleft aligncenter alignright alignfull alignjustify link unlink fullscreen code",
            "styleselect forecolor pastetext removeformat table charmap outdent indent undo redo image"
        ],
        relative_urls: false,
        remove_script_host: false
    });
    //TinyMCE

    //D A T A   T A B L E S
    $(".table-data").dataTable();
    //D A T A   T A B L E S

    $(".btn-trash").click(function(){
        return confirm("Are you sure want to trash this data?");
    });

    $(".fancy-iframe").fancybox({
		fitToView	: false,
		width		: '80%',
		height		: '80%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
