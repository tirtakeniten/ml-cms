<?php

$config['media'] = [
    'size' => [
        'thumb' => [
            'w' => 150,
            'h' => 150,
            'crop' => 1
        ],
        'small' => [
            'w' => 300,
            'h' => 300,
            'crop' => 0
        ],
        'medium' => [
            'w' => 600,
            'h' => 600,
            'crop' => 0
        ],
        'full' => [
            'w' => 0,
            'h' => 0,
            'crop' => 0
        ]
    ],

    /* relative to base_url */
    'url' => 'assets/img/',
];
