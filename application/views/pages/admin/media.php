<?php
    $CI = &get_instance();
?>
<div class="container-fluid" style="margin-top: 70px;">
    <input type="hidden" name="field" value="<?php echo $this->input->get('field'); ?>">
    <?php
        if ($CI->input->get('search')) :
    ?>
    <div class="alert alert-success">
        Search result for <?php echo $CI->input->get('search') ?>.
    </div>
    <?php
        endif;
    ?>
    <div class="row">
        <?php foreach($posts as $post): ?>
        <div class="col-xs-2">
            <div class="thumbnail">
                <img src="<?php echo $post->size['small']; ?>" alt="<?php echo $post->title; ?>" title="<?php echo $post->title; ?>">
                <div class="caption">
                    <p><a href="#" class="select-pict" role="button" data-id="<?php echo $post->id ?>" data-url="<?php echo $post->size['full'] ?>">Select</a></p>
                    <p><a href="#" class="" role="button">View</a></p>
                    <p><a href="#" class="" role="button">Delete</a></p>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
