<?php
    $ci = &get_instance();
    $ci->load->library('form_generator');
    $ci->load->library('option_lib');

    $form = $ci->form_generator;

    $fields = $setting->fields();
?>
<?php echo form_open(); ?>
<?php foreach ($fields as $fieldName => $field): ?>
    <div class="form-group">
        <?php echo $form->label($field['label'], $fieldName); ?>
        <?php
        	if ($ci->option->get($fieldName)) $field['default_value'] = $ci->option->get($fieldName);
        	echo $form->generate($fieldName, $field); 
        ?>
    </div>

<?php endforeach; ?>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-flat">Submit</button>
</div>
<?php echo form_close(); ?>
