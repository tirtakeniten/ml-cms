<?php
    $CI = &get_instance();
?>
<div class="form-group" style="display: none;">
    `id`, `user_id`, `title`, `content`, `excerpt`, `feature_image_id`, `status`, `comment_status`, `password`, `slug`, `seo_title`, `seo_description`, `type`, `created_at`, `published_at`, `sequence`, `parent_id`
    <pre>
        <?php print_r(@$post); ?>
    </pre>
</div>
<?php echo form_open(); ?>

<input type="hidden" value="<?php echo @$post->id; ?>" name="id">
<input type="hidden" value="<?php echo $post_setting['user_id']; ?>" name="user_id">
<input type="hidden" value="<?php echo $post_setting['type']; ?>" name="type">

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <?php foreach ($langs as $k => $lang): ?>
        <li class="<?php echo ($k == 0) ? "active" : ''; ?>"><a href="#lang-<?php echo $lang->slug; ?>" data-toggle="tab"><?php echo $lang->name; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="tab-content">
<?php foreach ($langs as $k => $lang): ?>
    <div class="tab-pane <?php echo ($k == 0) ? "active" : ''; ?>" id="lang-<?php echo $lang->slug; ?>">
        <h2><?php echo $lang->name; ?></h2>
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" name="title[<?php echo $lang->slug; ?>]" class="form-control" placeholder="" value="<?php echo @$post->title[ $lang->slug ] ?>">
        </div>
        <div class="form-group">
            <label for="">Content</label>
            <p>
                <a href="<?php echo admin_url('media') ?>" class="btn btn-default fancy-iframe" data-fancybox-type="iframe">Add Picture</a>
            </p>
            <textarea name="content[<?php echo $lang->slug; ?>]" class="form-control tinymce" rows="25" id="content-<?php echo $lang->slug; ?>"><?php echo @$post->content[ $lang->slug ] ?></textarea>
        </div>
        <div class="form-group">
            <label for="">Excerpt</label>
            <textarea name="excerpt[<?php echo $lang->slug; ?>]" class="form-control" rows="7"><?php echo @$post->excerpt[ $lang->slug ] ?></textarea>
        </div>
        <div class="form-group">
            <label for="">SEO Title</label>
            <input type="text" name="seo_title[<?php echo $lang->slug; ?>]" class="form-control" placeholder="" value="<?php echo @$post->seo_title[ $lang->slug ] ?>">
        </div>
        <div class="form-group">
            <label for="">SEO Description</label>
            <input type="text" name="seo_description[<?php echo $lang->slug; ?>]" class="form-control" placeholder="" value="<?php echo @$post->seo_description[ $lang->slug ] ?>">
        </div>
    </div>
<?php endforeach; ?>
</div>
<hr>
<h3>Additional Field <small>(No translation required)</small></h3>
<div class="form-group">
    <label for="">URL</label>
    <input type="slug" name="feature_image_id" class="form-control" placeholder="" value="<?php echo @$post->slug; ?>">
</div>
<div class="form-group">
    <label for="">Page parent</label>
    <?php echo form_dropdown ('parent_id', $post_dropdown, @$post->parent_id, 'class="form-control"'); ?>
</div>
<div class="form-group">
    <label for="">Feature Image</label>
    <input type="text" name="feature_image_id" class="form-control" placeholder="" value="" id="feature_image_id">
    <a href="<?php echo admin_url('media') ?>?field=feature_image_id" class="btn btn-default fancy-iframe" data-fancybox-type="iframe">Browse</a>
</div>
<!--div class="form-group">
    <label for="">Feature Image</label>
    <input type="text" name="feature_image_id" class="form-control" placeholder="" value="<?php echo @$post->feature_image_id; ?>">
</div-->
<div class="form-group">
    <label for="">Status</label>
    <?php echo form_dropdown ('status', $post_states, @$post->status, 'class="form-control"'); ?>
</div>
<div class="form-group" style="display: none;">
    <label for="">Comment Status</label>
    <input type="text" name="comment_status" class="form-control" placeholder="" value="<?php echo @$post->comment_status; ?>">
</div>
<div class="form-group" style="display: none;">
    <label for="">Password</label>
    <input type="password" name="password" class="form-control" placeholder="" value="<?php echo @$post->password; ?>">
</div>
<div class="form-group">
    <label for="">Page Order</label>
    <input type="number" name="sequence" class="form-control" placeholder="" value="<?php echo @$post->sequence; ?>">
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-flat">Submit</button>
</div>
<?php echo form_close(); ?>
