<div class="box-body no-padding">
    <table class="table table-hover table-data">
        <thead>
            <tr>
                <th>Title</th>
                <th>Status</th>
                <th>URL</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($posts as $post): ?>
            <tr>
                <td><?php echo $post->title; ?></td>
                <td><?php echo $post->status; ?></td>
                <td>
                    <a href="<?php echo site_url($post->slug); ?>">Go</a>
                </td>
                <td>
                    <a href="<?php echo admin_url('post/form/'."{$post->type}/{$post->id}") ?>" class="btn btn-info btn-flat btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a href="<?php echo admin_url('post/trash/'."{$post->type}/{$post->id}") ?>" class="btn btn-warning btn-flat btn-sm btn-trash"><i class="fa fa-trash-o"></i> Trash</a>
                    <?php if($post->status == 'trash'): ?>
                    <a href="<?php echo admin_url('post/restore/'."{$post->type}/{$post->id}") ?>" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-mail-reply-all"></i> Restore</a>
                    <a href="<?php echo admin_url('post/delete/'."{$post->type}/{$post->id}") ?>" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-times"></i> Delete</a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
