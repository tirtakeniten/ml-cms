<?php
  $CI = &get_instance();
  $user = $CI->kulkul_auth->user();

  $notifType = [
    'danger'  => 'error',
    'success' => 'success',
    'warning' => 'warning',
    'info'    => 'info'
  ];
?><!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Hotel CMS Admin Control Panel</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <?php echo $css; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <a href="../../index2.html" class="logo"><b>Hotel</b>CMS</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user"></i>
                  <?php echo $user->full_name; ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">Profile</a></li>
                  <li><a href="#">Change Password</a></li>
                  <li><a href="#">Sign Out</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i><span>Page</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo admin_url('post'); ?>"><i class="fa fa-circle-o"></i> All Pages</a></li>
                    <li><a href="<?php echo admin_url('post/form/page'); ?>"><i class="fa fa-circle-o"></i> Add Page</a></li>
                </ul>
            </li>
            <li>
              <a href="<?php echo admin_url('media'); ?>">
                <i class="fa fa-dashboard"></i> <span>Media</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-wrench"></i>
                <span>Setting</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo admin_url('option/form/general'); ?>"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="<?php echo admin_url('option/form/lang'); ?>"><i class="fa fa-circle-o"></i> Languange</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $page_title; ?>
            <small><?php echo $page_description; ?></small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <?php foreach ($notifType as $type => $notif) : ?>
                <?php if ($CI->session->flashdata($notif)): ?>
            <div class="alert alert-<?php echo $type ?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata($notif); ?>
              </div>
                <?php endif; ?>
            <?php endforeach; ?>

          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-body">

            <!-- NOTIFICATIONS -->

            <!-- NOTIFICATIONS -->

            <?php echo $content ?>

            </div><!-- /.box-body -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
      </footer>
    </div><!-- ./wrapper -->

    <?php echo $js; ?>
  </body>
</html>
