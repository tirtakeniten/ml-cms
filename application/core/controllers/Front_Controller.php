<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Front_Controller extends MY_Controller
{
    var $db_lang;

    public function __construct()
    {
        parent::__construct();
        ///$this->load->library();
        $this->load->model([
            'languange_model'
        ]);
        $this->define_lang();
    }

    protected function define_lang()
    {
        // if no parameter
        if (!$this->input->get('lang'))
        {
            if (!$this->session->userdata('language'))
            {
                // get default lang
                $langID = $this->option->get('default_lang');
                $langRow = $this->languange_model->get($langID);
                $this->session->set_userdata('language', $langRow->slug);
            }
        }

        // if parameter
        if ($this->input->get('lang'))
        {
            $lang = $this->input->get('lang');
            $langRow = $this->languange_model->get_by('slug', $lang);
            if(is_null($langRow)) redirect($this->uri->uri_string());
            $this->session->set_userdata('language', $langRow->slug);
        }

        $this->config->set_item('language', $this->session->userdata('language'));
    }
}
