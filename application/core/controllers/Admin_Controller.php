<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Admin_Controller extends MY_Controller
{
    public $view_prefix = 'admin/';

    public function __construct()
    {
        parent::__construct();

        $this->load->library('kulkul_auth');

        $this->_check_user();
        $this->_load_layout();
    }

    /**
     * Check if user is already login
     * and redirect to /user/index if not admin
     **/
    private function _check_user()
    {
        /* if already login */
        if($this->kulkul_auth->user())
        {
            /*$user = (object) $this->kulkul_auth->user();
            if($user->is_admin != 1)
            {
                show_error('You do not have sufficient permissions to access this page',
                    '403',
                    'Access Forbidden');
                exit();
            }*/
        }else{
            redirect('admin/auth/login');
        }
    }

    /**
     * Styles and scripts for admin is here
     * and also the layout
     **/
    private function _load_layout()
    {
        /* load style on controller */
        $this->stencil->css(array(
            'bootstrap.min.css',
            asset_url('plugins/admin-lte/css/AdminLTE.min.css'),
            asset_url('plugins/font-awesome/css/font-awesome.min.css'),
            asset_url('plugins/iCheck/square/blue.css'),
            asset_url('plugins/datatables/dataTables.bootstrap.css'),
            asset_url('plugins/admin-lte/css/skins/_all-skins.min.css'),
            asset_url('plugins/fancybox/jquery.fancybox.css')
        ));

        /* load script on controller */
        $this->stencil->js(array(
            'jQuery-2.1.3.min.js',
            'bootstrap.min.js',
            asset_url('plugins/iCheck/icheck.min.js'),
            asset_url('plugins/slimScroll/jquery.slimscroll.min.js'),
            asset_url('plugins/fastclick/fastclick.min.js'),
            asset_url('plugins/tinymce/tinymce.min.js'),
            asset_url('plugins/tinymce/jquery.tinymce.min.js'),
            asset_url('plugins/datatables/jquery.dataTables.min.js'),
            asset_url('plugins/datatables/dataTables.bootstrap.min.js'),
            asset_url('plugins/admin-lte/js/app.min.js'),
            asset_url('plugins/fancybox/jquery.fancybox.js'),
            'admin.js',
        ));

        $this->stencil->data([
            'page_title' => 'Admin Panel',
            'page_description' => ''
        ]);

        $this->stencil->layout('admin_layout');
    }

    private function _set_notif($type, $data)
    {
        return $this->session->set_flashdata($type, $data);
    }
}
