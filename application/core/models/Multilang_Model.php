<?php

class Multilang_Model extends MY_Model {

    public $lang;

    public $default_lang;

    public function __construct () {
        parent::__construct();

        $this->_default_lang();

        $this->set_before_create( (array) 'lang_encode' );
        $this->set_before_update( (array) 'lang_encode');
        $this->set_after_get([
            'lang_decode',
            'sepparate_lang'
        ]);
    }

    public function sepparate_lang ($row) {
        if (!is_null($row)) {
            if ($this->lang != '') {
                $row_array = $row;
                if(is_object($row_array)) $row_array = (array) $row;

                foreach ($row_array as $col => $val) {
                    if (isset($val[ $this->lang ])) $row_array[$col] = $val[$this->lang];
                }

                if(is_object($row)) $row = (object) $row_array;
            }
        }

        return $row;
    }

    /**
    * B E F O R E
    **/
    public function set_before_create ($callable) {
        $this->before_create = array_merge($callable, $this->before_create);
    }

    public function set_before_update ($callable) {
        $this->before_update = array_merge($callable, $this->before_update);
    }

    public function set_before_get ($callable) {
        $this->before_get = array_merge($callable, $this->before_get);
    }

    public function set_before_delete ($callable) {
        $this->before_delete = array_merge($callable, $this->before_delete);
    }

    /**
    * A F T E R
    **/
    public function set_after_create ($callable) {
        $this->after_create = array_merge($callable, $this->after_create);
    }

    public function set_after_update ($callable) {
        $this->after_update = array_merge($callable, $this->after_update);
    }

    public function set_after_get ($callable) {
        $this->after_get = array_merge($this->after_get, $callable);
    }

    public function set_after_delete ($callable) {
        $this->after_delete = array_merge($callable, $this->after_delete);
    }

    public function lang_encode ($row) {
        foreach ($row as $col => $val) {
            if (is_array($val)) $row[$col] = json_encode($val);
        }
        return $row;
    }

    public function lang_decode ($row) {
        if (is_null($row)) return $row;
        $row_array = $row;
        if(is_object($row_array)) $row_array = (array) $row;

        foreach ($row_array as $col => $val) {
            if($this->_isJSON($val)) $row_array[$col] = json_decode($val, true);
        }

        if(is_object($row)) $row = (object) $row_array;
        return $row;
    }

    private function _isJSON ($str) {
        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    protected function _default_lang () {
        $defaultLangID = $this->option->get('default_lang');
        $langData = $this->languange_model->get($defaultLangID);
        $this->default_lang = $langData->slug;
    }
}
