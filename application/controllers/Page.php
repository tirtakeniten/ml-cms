<?php

class Page extends Front_Controller
{
    var $type = 'page';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('post_model');
        $this->post_model->lang = $this->config->item('language');
    }

    public function index ($page = '', $sub_page = '')
    {
        $where = [
            'type' => $this->type,
            'status' => 'publish'
        ];

        //get the post
        $this->db->where($where);
        $data['page'] = $this->post_model->get_by(['slug' => $page]);

        //children
        $this->db->where($where);
        $data['children'] = $this->post_model->get_many_by(['parent_id' => $data['page']->id]);

        //parent
        $this->db->where($where);
        $data['parent'] = $this->post_model->get($data['page']->parent_id);

        print_r($data['page']);
    }
}
