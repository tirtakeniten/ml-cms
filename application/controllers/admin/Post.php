<?php

class Post extends Admin_Controller {

	const POST_CONFIG_PATH = '';
	const EXT = '.php';

	public $post_setting;

	public function __construct()
	{
		parent::__construct();
		$this->load->model([
			'languange_model',
			'post_model'
		]);

		$data['user'] = $this->kulkul_auth->user();
		$this->post_setting = [
			'user_id' => $data['user']->id,
			'type' => 'page'
		];
	}

	public function index ($type = 'page') {
		$data['page_title'] = 'List Page';
		$data['page_description'] = '';

		$this->post_model->lang = 'english';

		$this->db->where(['type' => $this->post_setting['type']]);

		$data['posts'] = $this->post_model->get_all();

		$this->stencil->data($data);

		$this->stencil->paint($this->view_prefix.'post');
	}

	public function form ($type = 'page', $id = NULL) {
		/* Language */
		$available_lang_id = $this->option->get('available_lang');
		$this->db->where_in('id', $available_lang_id);
		$data['langs'] = $this->languange_model->get_all();

		/* User */
		$data['user'] = $this->kulkul_auth->user();

		/* Title */
		$data['page_title'] = 'Page Form';
		$data['page_description'] = '';
		$data['post_setting'] = $this->post_setting;

		if ($id != NULL) $data['post'] = $this->post_model->get($id);

		$displayForm = function () use ($data, $type, $id){
			$data['post_dropdown'] = $this->post_model->parent_post($type, $id);
			$data['post_states'] = $this->post_model->post_states();

			$this->stencil->data($data);

			$this->stencil->paint($this->view_prefix.'post-form');
		};

		if ($this->input->method() == 'get') {
			$displayForm();
		} elseif ($this->input->method() == 'post') {

			$this->load->library('form_validation');

			$this->form_validation->set_rules('user_id', 'User ID', 'required|integer');
			$this->form_validation->set_rules('title[]', 'Title', 'required');
			$this->form_validation->set_rules('content[]', 'Content', 'required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', validation_errors());
				$data['post'] = (object) $this->input->post();
				$displayForm();
			} else {
				$post = [
					'user_id'		=> $this->input->post('user_id'),
					'title'			=> $this->input->post('title'),
					'content'		=> $this->input->post('content'),
					'excerpt'		=> $this->input->post('excerpt'),
					'feature_image_id' => $this->input->post('feature_image_id'), //$this->input->post('feature_image_id'),
					'status'		=> $this->input->post('status'),
					'comment_status'=> 'close', //$this->input->post('comment_status'),
					'password'		=> '', //$this->input->post('password'),
					'slug'			=> $this->input->post('slug'),
					'seo_title'		=> $this->input->post('seo_title'),
					'seo_description' => $this->input->post('seo_description'),
					'type'			=> $this->input->post('type'),
					'created_at'	=> $this->input->post('created_at'),
					'published_at'	=> $this->input->post('published_at'),
					'sequence'		=> $this->input->post('sequence'),
					'parent_id'		=> $this->input->post('parent_id'),
				];

				if (is_null($id)) {
					$this->post_model->insert($post);
				} else {
					$this->post_model->update($this->input->post('id'), $post);
				}

				$this->session->set_userdata('success', 'Post have been saved.');
				redirect(admin_url('post/index/'.$type));
			}
		} else {
			show_404;
		}
	}

	/**
	* Trash
	**/
	public function trash ($type = 'page', $id) {
		$post = $this->post_model->get_by([
			'type' => $type,
			'id' => $id,
			'status != ' => 'trash'
		]);

		if (is_null($post)) {

		} else {
			$this->session->set_flashdata('success', 'Data has been sent to trash.');
		}

		redirect(admin_url('post/index/'.$type));
	}

	/**
	* Trash
	**/
	public function restore ($type = 'page', $id) {

	}

	/**
	* Trash
	**/
	public function delete ($type = 'page', $id) {

	}
}
