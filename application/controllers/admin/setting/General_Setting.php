<?php

require_once APPPATH.'controllers/admin/setting/OptionInterface.php';

class General_Setting implements OptionInterface{

	function fields()
	{
		return [
			'site_name' => [
				'type' => 'text',
				'attribute' => [
					'class' => 'control-site_name',
					'placeholder' => 'Site Name'
				],
				'label' => 'Website Name',
				'validation' => [
					'rules' => 'required'
				],
				'default_value' => 'Hotel Name'
			],
			'tagline' => [
				'type' => 'text',
				'attribute' => [
					'placeholder' => 'Website Tagline',
				],
				'label' => 'Website Tagline',
				'validation' => [
					'rules' => 'required'
				],
				'default_value' => 'Hotel Name'
			],
			'date_format' => [
				'type' => 'select',
				'attribute' => [
					'placeholder' => 'Website Tagline',
				],
				'label' => 'Date Format',
				'validation' => [
					'rules' => 'required'
				],
				'default_value' => 'Y-m-d',
				'options' => $this->date_options()
			]
		];
	}

	function title()
	{
		return "General Setting";
	}

	function description()
	{
		return "Basic configuration of your site";
	}

	function view()
	{
		return NULL;
	}

	function date_options()
	{
		$formats = [
			'j F Y'	,
			'F jS, Y',
			'j/n/Y',
			'n/j/Y',
			'Y-m-d'
		];

		$list = [];

		foreach ($formats as $format) {
			$list[$format] = date($format);
		}

		return $list;
	}
}
