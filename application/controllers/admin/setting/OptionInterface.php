<?php

/**
* The iterface of site option
**/
interface OptionInterface {

	/**
	* The fields, e.g:
	* [ 'site_name' => [
	*		'type' => 'text',
	*		'attribute' => [
	*			'class' => 'control-site_name'
	*			'place_holder' => 'Site Name',
	*		],
	*		'label' => 'Website Name',
	*		'validation' => [
    *           'rules' => 'required',
    *           'errors' => array(
    *                    'required' => 'You must provide a %s.',
    *            ),
	*		]
	*	]
	* ]
	**/
	function fields();

	/**
	* Title of the page
	**/
	function title();

	/**
	* Description
	**/
	function description();

	/**
	* view / template of setting
	**/
	function view();
}