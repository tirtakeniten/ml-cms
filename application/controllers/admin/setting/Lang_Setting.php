<?php

require_once APPPATH.'controllers/admin/setting/OptionInterface.php';

class Lang_Setting implements OptionInterface{

	var $ci;

	public function __construct() 
	{
		$this->ci = &get_instance();
		$this->ci->load->model('languange_model');
	}

	function fields()
	{
		return [
			'default_lang' => [
				'type' => 'select',
				'attribute' => [
					'placeholder' => 'Default Languange'
				],
				'label' => 'Default Languange',
				'validation' => [
					'rules' => 'required'
				],
				'default_value' => '1',
				'options' => $this->lang_dropdown()
			],
			'available_lang' => [
				'type' => 'checkbox',
				'attribute' => [
				],
				'label' => 'Available Language',
				'validation' => [
					'rules' => 'required'
				],
				'default_value' => '1',
				'options' => $this->lang_dropdown()
			]
		];
	}

	function title()
	{
		return "Languange Setting";
	}

	function description()
	{
		return "";
	}

	function view()
	{
		return NULL;
	}

	public function lang_dropdown () {
		return $this->ci->languange_model->dropdown('id', 'name');
	}
}
