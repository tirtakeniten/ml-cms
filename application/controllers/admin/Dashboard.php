<?php

class Dashboard extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->stencil->paint($this->view_prefix.'dashboard');
	}

}