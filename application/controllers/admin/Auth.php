<?php

class Auth extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index ()
	{

	}

	public function login()
	{
		if ($this->input->method() == 'post')
		{
			$this->load->library('kulkul_auth');

			$this->kulkul_auth->set_value([
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
			]);

			$valid = $this->kulkul_auth->start();

			if(!$valid) {
				
			} else {
				redirect('admin');
			}
		}

		$this->stencil->css([
			'bootstrap.min.css',
			asset_url('plugins/font-awesome/font-awesome.min.css'),
			asset_url('plugins/admin-lte/css/AdminLTE.min.css'),
			asset_url('plugins/iCheck/square/blue.css')
		]);

		$this->stencil->js([
			'jQuery-2.1.3.min.js',
			'bootstrap.min.js',
			asset_url('plugins/iCheck/icheck.min.js')
		]);

		$this->stencil->layout('plain_layout');

		$this->stencil->paint('admin/auth-login');
	}

	public function seed_user()
	{	
		$this->load->library('encrypt');
		/*
		$this->db->insert(
			'users',
			[
				'username'	=> 'admin',
				'email'		=> 'admin@website.com',
				'full_name'	=> 'Administrator',
				'password'	=> $this->encrypt->encode('admin')
			]
		);
		*/
	}
}