<?php

class Option extends Admin_Controller {

	const EXT = '.php';
	const SETTING_PATH = '/controllers/admin/setting/';

	public function __construct()
	{
		parent::__construct();
	}

	public function form($setting = 'general')
	{
		// Class Name
		$fileClassName = ucfirst($setting) . '_Setting';

		// Filename
		$fileName = $fileClassName . self::EXT;

		// Absolute Path
		$filePath = APPPATH . self::SETTING_PATH . $fileName;

		// Load File
		$file = $this->load->file($filePath);

		// Construct
		$settingClass = new $fileClassName;

		/* OK, do as ussual */
        $data['setting'] = $settingClass;
		$data['page_title'] = $settingClass->title();
		$data['page_description'] = $settingClass->description();

        /* the view vars */
        if ($settingClass->view() == NULL) {
            $view_file = $this->view_prefix.'option-form';
        }else{
            $view_file = $this->view_prefix . $settingClass->view();
        }

        $form_view = function () use ($data, $view_file) {
            $this->stencil->data($data);
    		$this->stencil->paint($view_file);
        };

		if ($this->input->method() == 'post') {

            $this->load->library('form_validation');

            $fields = $settingClass->fields();

            foreach($fields as $name => $field) {
                $rules[$name] = $field['validation'];

                if ($field['type'] == 'checkbox') {
                	if(!isset($rules[$name]['field'])) $rules[$name]['field'] = $name.'[]';
                } else {
                	if(!isset($rules[$name]['field'])) $rules[$name]['field'] = $name;
                }

                if(!isset($rules[$name]['label'])) $rules[$name]['label'] = $field['label'];

            }

            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == FALSE) {
                $form_view();
            } else {
                foreach ($this->input->post()  as $option_name => $value) {
                	$this->option->set($option_name, $value, true);
                }
                redirect( current_url() );
            }

		} else {
            $form_view();
		}
	}
}
