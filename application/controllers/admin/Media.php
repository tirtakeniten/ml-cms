<?php

use Gregwar\Image\Image;

class Media extends Admin_Controller {

    public $post_setting;

    public $uploadConfig;

    public function __construct () {
        parent::__construct();
        $this->stencil->layout('media_layout');
        $this->config->load('media');
        $this->load->model([
			'languange_model',
			'post_model'
		]);

        $this->uploadConfig = $this->config->item('media');

        $data['user'] = $this->kulkul_auth->user();
        $this->post_setting = [
			'user_id' => $data['user']->id,
			'type' => 'media'
		];
    }

    public function index () {
        if ($this->input->get('search')) {
            $this->db->like('title', $this->input->get('search'));
            $this->db->or_like('content', $this->input->get('search'));
        }
        $this->db->where(['type' => $this->post_setting['type']]);
		$data['posts'] = $this->post_model->order_by('published_at', 'desc')->get_all();
        $this->stencil->data($data);
        $this->stencil->paint($this->view_prefix.'media');
    }

    public function upload () {
        if ($this->input->method() == 'get') {
            $this->stencil->paint($this->view_prefix.'media-upload');
        } else if($this->input->method() == 'post') {

            $config['upload_path']          = FCPATH.$this->uploadConfig['url'];
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 3*1024;
            $config['max_width']            = 5000;
            $config['max_height']           = 5000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                $data = array('error' => $this->upload->display_errors());
            }
            else
            {
                $data = $this->upload->data();

                $this->resize($data);

                $post = [
					'user_id'		=> $this->post_setting['user_id'],
					'title'			=> $data['raw_name'],
					'content'		=> json_encode($data),
					'excerpt'		=> '',
					'feature_image_id' => 0,
					'status'		=> 'publish',
					'comment_status'=> 'close',
					'password'		=> '',
                    'slug'          => $data['raw_name'],
					'seo_title'		=> $data['raw_name'],
					'seo_description' => $data['raw_name'],
					'type'			=> $this->post_setting['type'],
					'sequence'		=> 0,
					'parent_id'		=> 0,
				];
                $this->post_model->insert($post);
            }

            echo json_encode($data);
        }
    }

    public function edit () {

    }

    public function delete () {

    }

    private function resize ($uploadData = '') {

        $source = $uploadData['full_path'];
        $path = $uploadData['file_path'];
        $fileName = $uploadData['orig_name'];

        $mediaSize = $this->uploadConfig['size'];

        foreach ($mediaSize as $folder => $size) {

            $full_resize_name = "{$path}{$folder}/{$fileName}";

            if ($size['crop']) {
                Image::open($source)
                         ->zoomCrop($size['w'], $size['h'])
                         ->save($full_resize_name);
            } else {
                Image::open($source)
                         ->cropResize($size['w'], $size['h'])
                         ->save($full_resize_name);
            }
        }
    }
}
