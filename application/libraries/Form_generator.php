<?php

class Form_generator {

    protected $_ci;

    public function __construct() {
        $this->_ci = &get_instance();
        $this->_ci->load->helper('form');
    }

    public function generate ($controlname, $data = []) {
        if ($data['type'] == 'text') {

            $attr = $data['attribute'];

            $attr['value'] = set_value($controlname, $data['default_value']);
            $attr['name'] = $controlname;
            $attr['class'] = 'form-control '.@$attr['class'];

            return $this->text($attr);
        } elseif ($data['type'] == 'select') {

            $option = $data['options'];
            $selected = $data['default_value'];
            $extra['class'] = 'form-control '.@$data['attribute']['class'];

            return $this->select($controlname, $option, $selected, $extra);
        } elseif ($data['type'] == 'checkbox') {
            $cb = '';
            $option = $data['options'];
            foreach ($option as $key => $cbData) {
                $attr = [
                    'name'          => $controlname.'[]',
                    'value'         => $key,
                    'checked'       => in_array($key, $data['default_value']),
                    'class'         => ''
                ];
                $cb .= $this->checkbox($attr, $cbData);
            }
            return $cb;
        }
    }

    public function text ($data) {
        return form_input( $data);
    }

    public function select (
        $name,
        $option = [],
        $selected = [],
        $extra = '')
    {
        return form_dropdown($name, $option, $selected, $extra);
    }

    public function checkbox ($data, $label) {
        $checkbox = '<div class="checkbox">';
        $checkbox .= '<label>';
        $checkbox .= form_checkbox($data);
        $checkbox .= ' '.$label.'</label>';
        $checkbox .= '</div>';
        return $checkbox;
    }

    public function label($label, $for = '') {
        return form_label($label, $for);
    }
}
