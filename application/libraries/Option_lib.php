<?php

class Option_lib {
    
    const TABLE = 'options';

    protected $_ci;
    protected $_db;
    protected $_config;
    
    public function __construct() {
        $this->_ci = &get_instance();
        $this->_db = $this->_ci->db;
        $this->_config = $this->_ci->config;
    }
    
    public function get ($name) {
        /* check the CI config first */
        if ($this->_config->item($name)) {
            return $this->_config->item($name);
        } else {
        /* no data on CI config */
            $option = $this->_get($name);

            if ($option == NULL) return FALSE;
            
            if ($this->_isJSON($option->option_value)) 
                $option->option_value = $this->_unserialize($option->option_value);

            /* set the config */
            $this->_config->set_item($name, $option->option_value);

            return $option->option_value;
            //return $this->get($name);
        }
    }
    
    public function set ($name, $value, $save = false) {
        /* if we need to save the option value into database */
        if ($save) {
            if (!$this->_is_exist($name)) {
                $this->_insert($name, $value);
            }
            $this->_update($name, $value);
        } else {
        /* if no */
            $this->_config->set_item($name, $value);
        }
    }
    
    private function _insert ($name, $value) {
        if (is_array($value)) $value = $this->_serialize($value);

        return $this->_db->insert(self::TABLE, [
            'option_key' => $name,
            'option_value' => $value,
        ]);
    }
    
    private function _get ($name) {
        return $this->_db->get_where(self::TABLE, [
            'option_key' => $name
        ])->row();
    }
    
    private function _update ($name, $value) {
        if (is_array($value)) $value = $this->_serialize($value);

        $this->_db->where([
            'option_key' => $name
        ]);
        return $this->_db->update(self::TABLE, [
            'option_value' => $value
        ]);
    }
    
    private function _delete ($name) {
        
    }

    /*
    |============================================
    | H E L P E R   B L O C K
    |============================================
    */
    private function _is_exist ($name) {
        $option = $this->_get($name);
        if (is_null($option)) return FALSE;
        return TRUE;
    }

    private function _serialize($value = []) {
        if (is_array($value) || is_object($value)) $value = json_encode($value);
        return $value;
    }

    private function _unserialize($value) {
        if ( $this->_isJSON($value) ) return json_decode($value, true);
        return $value;
    }

    private function _isJSON ($str) {
        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}