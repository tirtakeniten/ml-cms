<?php

if( ! function_exists( 'admin_url' ) ) {
    function admin_url($uri = '', $protocol = NULL)
	{
		return site_url('admin/'.$uri, $protocol);
	}
}
