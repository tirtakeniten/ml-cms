<?php

class Post_model extends Multilang_Model {

    var $_table = 'posts';

    public function __construct () {
        parent::__construct();
        parent::set_before_update( ['remove_base_url', 'slugify'] );
        parent::set_before_create( ['remove_base_url', 'slugify'] );
        $this->set_after_get( ['add_base_url', 'render_media'] );
        $this->config->load('media');
    }

    public function slugify ($row) {
        if (!isset($row['slug'])
            || empty($row['slug'])
            || is_null($row['slug'])) {

            $row['slug'] = $row['title'][ $this->default_lang ];
        }
        $row['slug'] = url_title($row['slug'], '-', true);
        return $row;
    }

    public function parent_post ($type = 'page', $exclude = 0) {
        $posts = $this->get_many_by([
            'id != ' => $exclude,
            'type' => $type
        ]);
        $default_lang = $this->default_lang;

        $post_dropdown = ['0' => '-- No Parrent --'];
        foreach ($posts as $post) {
            $post_dropdown[$post->id] = $post->title[$default_lang];
        }

        return $post_dropdown;
    }

    public function post_states () {
        return [
            'publish' => 'Publish',
            'draft' => 'Draft',
            'trash' => 'Trash'
        ];
    }

    public function render_media ($row) {
        if (!is_null($row)) {
            if (isset($row->type) && $row->type == 'media') {
                $media_size = $this->config->item('media')['size'];
                $media_url = $this->config->item('media')['url'];
                $return = [];
                foreach ($media_size as $folder => $size) {
                    $return[$folder] = base_url("{$media_url}{$folder}/".$row->content['file_name']);
                }
                $row->size = $return;
            }
        }

        return $row;
    }

    public function remove_base_url ($row) {
        if (isset($row['content'])) {
            foreach ($row['content'] as $lang => $content) {
                $row['content'][$lang] = str_replace( base_url(), '[base_url]', $content );
            }
        }
        return $row;
    }

    public function add_base_url ($row) {
        $type = (is_array($row)) ? 'array' : 'object';

        if ($type == 'object') $row = (array) $row;
        if (isset($row['content'])) {
            if (is_array($row['content'])) {
                foreach ($row['content'] as $lang => $content) {
                    $row['content'][$lang] = str_replace( '[base_url]', base_url(), $row['content'][$lang] );
                }
            }else{
                $row['content'] = str_replace( '[base_url]', base_url(), $row['content'] );
            }
        }
        if ($type == 'object') $row = (object) $row;
        return $row;
    }
}
